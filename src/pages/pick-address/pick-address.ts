import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EnderecoDTO } from '../../models/endereco.dto';
import { ClienteService } from '../../service/domain/cliente.service';
import { StorageService } from '../../service/storage.service';
import { PedidoDTO } from '../../models/pedido.dto';
import { CartService } from '../../service/domain/cart.service';

@IonicPage()
@Component({
  selector: 'page-pick-address',
  templateUrl: 'pick-address.html',
})
export class PickAddressPage {

  enderecos: EnderecoDTO[];
  pedido: PedidoDTO;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public clienteService: ClienteService,
    public storage: StorageService,
    public cartService: CartService
    ) {
  }

  ionViewDidLoad() {
    this.showEnderecos();
  }

  showEnderecos(){
    let localUser = this.storage.getLocalUser();
    if(localUser && localUser.email){
      this.clienteService.findByEmail(localUser.email)
        .subscribe(response => {
          this.enderecos = response['enderecos'];

          let cart = this.cartService.getCart();

          this.pedido = {
            cliente: {id: response['id']},
            enderecoDeEntrega: null,
            pagamento: null,
            itens: cart.items.map(x => {return {produto: {id: x.produto.id}, quantidade: x.quantidade}})
          }
        },
        error => {
          if(error.status == 403){
            this.navCtrl.setRoot('HomePage');
          }
        })
    }
    else{
      this.navCtrl.setRoot('HomePage');
    }
  }

  nextPage(endereco: EnderecoDTO){
    this.pedido.enderecoDeEntrega = {id: endereco.id};
    this.navCtrl.push('PaymentPage', {pedido: this.pedido});
  }
}
