import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaService } from '../../service/domain/categoria.service';
import { CategoriaDTO } from '../../models/categoria.dto';
import { API_CONFIG } from "../../config/api.config";
import { MenuController } from '../../../node_modules/ionic-angular/components/app/menu-controller';


@IonicPage()
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {

  bucketUrl : string = API_CONFIG.bucketUrl
  items: CategoriaDTO[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public categoriaService: CategoriaService,
    public menu: MenuController) {
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(true);
    this.categoriaService.findAll()
      .subscribe(response => {
        this.items = response
      },
    error => {});
  }

  showProdutos(categoriaId : string){
    this.navCtrl.push('ProdutosPage', {categoriaId : categoriaId});
  }

}
