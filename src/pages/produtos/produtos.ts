import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ProdutoDTO } from '../../models/produto.dto';
import { ProdutoService } from '../../service/domain/produto.service';
import { API_CONFIG } from '../../config/api.config';

@IonicPage()
@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html',
})
export class ProdutosPage {

  items: ProdutoDTO[] = [];
  page : number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ProdutoService,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.loadData();
  }

  loadData(){
    let categoriaId = this.navParams.get('categoriaId');

    let loader = this.presentLoading();

    this.service.findByCatagoria(categoriaId, this.page, 10)
      .subscribe(response =>{
        let start = this.items.length;
        this.items = this.items.concat(response['content']);
        let end = this.items.length -1;
        loader.dismiss();
        console.log(this.items);
        console.log(this.page);
        this.loadImageUrls(start, end);
      },
      error =>{
        loader.dismiss();
      });
  }

  loadImageUrls(start: number, end: number){
    for(var i=start; i<=end; i++){
      let item = this.items[i];
      this.service.getSmallImageFromBucket(item.id)
        .subscribe(response =>{
          item.imageUrl = `${API_CONFIG.bucketUrl}/prod${item.id}-small.jpg`;
        },
        error =>{});
    }
  }

  produtoDetail(produtoId : string){
    this.navCtrl.push('ProdutoDetailPage', {produtoId : produtoId});
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  doRefresh(refresher) {
    this.items = []
    this.page = 0;
    this.loadData();
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll) {
    this.page ++;
    this.loadData()
    setTimeout(() => {
      infiniteScroll.complete();
    }, 500);
  }
}
