import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { EstadoService } from '../../service/domain/estado.service';
import { EstadoDTO } from '../../models/estado.dto';
import { CidadeDTO } from '../../models/cidade.dto';
import { CidadeService } from '../../service/domain/cidade.service';
import { ClienteService } from '../../service/domain/cliente.service';
import { AlertController } from '../../../node_modules/ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  formGroup: FormGroup
  estados: EstadoDTO[];
  cidades: CidadeDTO[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public estadoService : EstadoService,
    public cidadeService: CidadeService,
    public menu: MenuController,
    public clienteService: ClienteService,
    public alertCtrl: AlertController) {

      this.formGroup = this.formBuilder.group({

        nome: ["Diego da Silva", [Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
        email: ["erp.diego.silva@gmail.com", [Validators.required, Validators.email]],
        tipo: ["1", [Validators.required]],
        cpfOuCnpj: ["20660730910", [Validators.required, Validators.minLength(11), Validators.maxLength(14)]],
        senha: ["111", [Validators.required]],
        logradouro: ["Pioniru", [Validators.required]],
        numero: ["37", [Validators.required]],
        complemento: ["APT 4", []],
        bairro: ["", []],
        cep:["43401", [Validators.required]],
        telefone1:["721546977", [Validators.required]],
        telefone2:["", []],
        telefone3:["", []],
        estadoId:[null, [Validators.required]],
        cidadeId:[null, [Validators.required]]

      });

  }

  ionViewDidLoad(){
    this.estadoService.findAll()
      .subscribe(response =>{
        this.estados = response;
        this.formGroup.controls.estadoId.setValue(this.estados[0].id);
        this.updateCidade();
      },
      error => {
        console.log("Erro")
      })
  }

  signupUser(){
    console.log(this.formGroup.value);
    this.clienteService.insert(this.formGroup.value)
      .subscribe(response =>{
        this.showInserOK()
      },
    error =>{});
  }

  updateCidade(){
    let estado_id = this.formGroup.value.estadoId;
    this.cidadeService.findAll(estado_id)
      .subscribe(response => {
        this.cidades = response;
        this.formGroup.controls.cidadeId.setValue(null);
      },
    error =>{})
  }

  showInserOK(){
    const alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Cadastro efetuado com sucesso',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () =>{
            this.navCtrl.pop();
          } 
        }
      ]
  });
    alert.present();
  }
}
