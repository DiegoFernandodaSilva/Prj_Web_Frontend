import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProdutoDTO } from '../../models/produto.dto';
import { ProdutoService } from '../../service/domain/produto.service';
import { API_CONFIG } from '../../config/api.config';
import { CartService } from '../../service/domain/cart.service';

/**
 * Generated class for the ProdutoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-produto-detail',
  templateUrl: 'produto-detail.html',
})
export class ProdutoDetailPage {

  produto: ProdutoDTO;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ProdutoService,
    public cartService: CartService) {
  }

  ionViewDidLoad() {
    this.produtoShow();
  }

  produtoShow(){
    let produtoId = this.navParams.get('produtoId')
    this.service.finById(produtoId)
      .subscribe(response => {
        this.produto = response;
        this.loadImageUrls();
      },
      error =>{});
  }

  loadImageUrls(){
    this.service.getImageFromBucket(this.produto.id)
      .subscribe(response =>{
        this.produto.imageUrl = `${API_CONFIG.bucketUrl}/prod${this.produto.id}.jpg`;
      },
      error =>{});
  }

  addToCart(produto: ProdutoDTO){
    this.cartService.addProduto(produto);
    this.navCtrl.setRoot("CartPage");
  }
}
