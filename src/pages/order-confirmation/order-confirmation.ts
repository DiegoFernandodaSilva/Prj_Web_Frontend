import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PedidoDTO } from '../../models/pedido.dto';
import { CartItem } from '../../models/cartItem';
import { ClienteDTO } from '../../models/cliente.dto';
import { EnderecoDTO } from '../../models/endereco.dto';
import { ClienteService } from '../../service/domain/cliente.service';
import { CartService } from '../../service/domain/cart.service';
import { PedidoService } from '../../service/domain/pedido.service';

@IonicPage()
@Component({
  selector: 'page-order-confirmation',
  templateUrl: 'order-confirmation.html',
})
export class OrderConfirmationPage {

  pedido: PedidoDTO;
  cartItems: CartItem[];
  cliente: ClienteDTO;
  endereco: EnderecoDTO;
  codPedido: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public clienteService: ClienteService,
    public cartService: CartService,
    public pedidoService: PedidoService) {

    this.pedido = this.navParams.get('pedido');
  
  }

  ionViewDidLoad() {
    this.cartItems = this.cartService.getCart().items;
    
    this.clienteService.findById(this.pedido.cliente.id)
      .subscribe(response =>{
        this.cliente = response as ClienteDTO;
        this.endereco = this.findEndereco(this.pedido.enderecoDeEntrega.id, response['enderecos']);
      },
      error =>{
        this.navCtrl.setRoot('HomePage');
      });
  }

  findEndereco(id: string, list: EnderecoDTO[]): EnderecoDTO{
    let position = list.findIndex(x => x.id == id );
    return list[position];
  }

  total(){
    return this.cartService.total();
  }

  checkout(){
    this.pedidoService.insert(this.pedido)
      .subscribe(response =>{
        this.cartService.createOrClearCart();
        this.codPedido = this.extractPedidoId(response.headers.get('location'));
        console.log(this.pedido);
      },
      error =>{
        if(error.status == 403) {
          this.navCtrl.setRoot('HomePage');
        }
      });
  }

  back(){
    this.navCtrl.setRoot('CartPage')
  }

  extractPedidoId(location: string): string{
    let position = location.lastIndexOf('/');
    return location.substring(position +1, location.length);
  }

  home(){
    this.navCtrl.setRoot('CategoriasPage');
  }
}
