import { Injectable } from "../../../node_modules/@angular/core";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { EstadoDTO } from "../../models/estado.dto";
import { Observable } from "../../../node_modules/rxjs";
import { API_CONFIG } from "../../config/api.config";

@Injectable()
export class EstadoService {
  
    constructor(public http : HttpClient){
    }

    findAll(): Observable<EstadoDTO[]> {
        return this.http.get<EstadoDTO[]>(`${API_CONFIG.baseUrl}/estados`);
    }
}