import { Injectable } from "../../../node_modules/@angular/core";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { Observable } from "../../../node_modules/rxjs";
import { ProdutoDTO } from "../../models/produto.dto";
import { API_CONFIG } from "../../config/api.config";

@Injectable()
export class ProdutoService{

    constructor(public http: HttpClient){
    }

    findByCatagoria(categoriaId: string, page: number = 0, linePerPage: number = 24){
        return this.http.get(`${API_CONFIG.baseUrl}/produtos/?categorias=${categoriaId}&page=${page}&linePerPage=${linePerPage}`);
    }

    finById(produtoId: string){
        return this.http.get<ProdutoDTO>(`${API_CONFIG.baseUrl}/produtos/${produtoId}`);
    }

    getSmallImageFromBucket(produtoId: string): Observable<any>{
        let url = `${API_CONFIG.bucketUrl}/prod${produtoId}-small.jpg`;
        return this.http.get(url, {responseType: 'blob'});
    }

    getImageFromBucket(id: string): Observable<any>{
        let url = `${API_CONFIG.bucketUrl}/prod${id}.jpg`;
        return this.http.get(url, {responseType: 'blob'});
    }
}