import { Injectable } from "../../node_modules/@angular/core";
import { LocalUser } from "../models/local_user";
import { Storage_Keys } from "../config/storage_keys.config";
import { Cart } from "../models/cart";

@Injectable()
export class StorageService{

    getLocalUser(): LocalUser{
        let usr = localStorage.getItem(Storage_Keys.localUser)
        if(usr == null){
            return null;
        }
        else{
            return JSON.parse(usr);
        }
    }

    setLocalUser(obj: LocalUser){
        if(obj == null){
            localStorage.removeItem(Storage_Keys.localUser);
        }
        else{
            localStorage.setItem(Storage_Keys.localUser, JSON.stringify(obj));
        }
    }

    getCart(): Cart{
        let cart = localStorage.getItem(Storage_Keys.cart)
        if(cart == null){
            return null;
        }
        else{
            return JSON.parse(cart);
        }
    }

    setCart(obj: Cart){
        if(obj == null){
            localStorage.removeItem(Storage_Keys.cart);
        }
        else{
            localStorage.setItem(Storage_Keys.cart, JSON.stringify(obj));
        }
    }
}