import { refDTO } from "./ref.dto";
import { PagamentoDTO } from "./pagamento.dto";
import { ItemPedidoDTO } from "./item-pedido.dto";

export interface PedidoDTO{
    cliente: refDTO;
    enderecoDeEntrega: refDTO;
    pagamento: PagamentoDTO;
    itens: ItemPedidoDTO[];
}