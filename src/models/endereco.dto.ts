import { CidadeDTO } from "./cidade.dto";

export interface EnderecoDTO{
    id: string;
    logradouro: string;
    numero: String;
	complemento: String;
	bairro: String;
    cep: String;
    cidade: CidadeDTO;
}